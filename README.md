# CSharp Python Functions

A library for CSharp that add Python style functions

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Installing

1. Fork the repository
2. Open the SLN file with your favourite CSharp editor
3. Start coding

For running tests of the code, the easiest method is to have a second project that relies on CSharp Python Functions and use the functions in that.

## Running the tests

Build the 'CSharp-Python-Functions-Tests' project
Run the tests that are in that project through your ide.  In Visual Studio this is done by going to the build menu and entering the test explorer.

## Deployment

Compile the DLL file and add it as a project dependancy.

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/ThomasSaunders/CSharp-Python-Functions/releases).

## Authors

* **Thomas Saunders** - *Initial work* - [thomas379](https://github.com/thomassaunders)

See also the list of [contributors](CONTRIBUTORS.md) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details